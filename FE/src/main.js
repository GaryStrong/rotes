import Vue from 'vue'
import router from './router'

//Bootstrap
Vue.use(BootstrapVue)
import BootstrapVue from 'bootstrap-vue/dist/bootstrap-vue.esm';
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import App from './App'

//Navigations
import NavBar from './components/navigations/NavBar.vue'
Vue.component('navigation', NavBar)

import SideBar from './components/navigations/SideBar.vue'
Vue.component('sidebar', SideBar)

//CSS
require('./assets/css/style.css')

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
