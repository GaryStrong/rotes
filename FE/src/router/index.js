import Vue from 'vue'
import VueRouter from 'vue-router'
import dash from '../components/pages/Dashboard.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Dashboard',
    component: dash
  },
  {
    path: '/dash',
    name: 'Dashboard',
    component: dash
  },

  {
    path: '/patron',
    name: 'Patron',
    component: () => import(/* webpackChunkName: "patron" */ '../components/pages/Patron.vue')
  },
  {
    path: '/book',
    name: 'Patron',
    component: () => import(/* webpackChunkName: "book" */ '../components/pages/Book.vue')
  },
  {
    path: '/settings',
    name: 'Settings',
    component: () => import(/* webpackChunkName: "settings" */ '../components/pages/Setting.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
export default router
